<?php

namespace app\components;

use yii\base\Component;
use yii\base\Model;
use yii\base\BaseObject;
use yii\httpclient\Client;
use yii\helpers\Json;

class NaviComponent extends Component
{

    static $version_ = "1.5";

    public function GetNaviaddress(string $version, string $container, string $naviaddress, string $lang = null, string $xUserLanguage = null, string $apiVersion = null)
    {
        $client = new Client();
        $request = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('https://staging-api.naviaddress.com/api/v' . $version . '/Addresses/' . $container . '/' . $naviaddress)
            ->addHeaders(['content-type' => 'application/json']);
        if (!is_null($lang)) {
            $request->addData(['lang' => $lang]);
        }
        if (!is_null($xUserLanguage)) {
            $request->addHeaders(['X-User-Language' => $xUserLanguage]);
        }
        if (!is_null($apiVersion)) {
            $request->addHeaders(['api-version' => $apiVersion]);
        }
        $response = $request->send();
        $resultObject = new ResultObject;
        $resultModel = null;
        if ($response->isOk) {
            $resultModel = new GetAddressModel;
        } else {
            $resultModel = new BaseResponse;
        }
        $resultModel->attributes = $response->data;
        if ($resultModel->validate()) {
            $resultObject->isOk = true;
            $resultObject->result = $resultModel;
        } else {
            $resultObject->result = new BaseResponse;
            $resultObject->result->message = "Validation of the model failed. ";
        }
        return $resultObject;
    }

    public function UpdateNaviaddressInfo(string $version, string $container, string $naviaddress, string $lang = null, $updateAddressModel = null, string $apiVersion = null)
    {
        $client = new Client();
        $request = $client->createRequest()
            ->setMethod('PUT')
            ->setUrl('https://staging-api.naviaddress.com/api/v' . $version . '/Addresses/' . $container . '/' . $naviaddress)
            ->addHeaders(['content-type' => 'application/json']);
        if (!is_null($lang)) {
            $request->addData(['lang' => $lang]);
        }
        if (!is_null($updateAddressModel)) {
            $array = $updateAddressModel->toArray();
            $request->setContent(Json::encode($array));
        }
        if (!is_null($apiVersion)) {
            $request->addHeaders(['api-version' => $apiVersion]);
        }
        $response = $request->send();
        $resultObject = new ResultObject;
        $resultModel = null;
        if ($response->isOk) {
            $resultModel = new NaviaddressModel;
        } else {
            $resultModel = new BaseResponse;
        }
        $resultModel->attributes = $response->data;
        if ($resultModel->validate()) {
            $resultObject->isOk = true;
            $resultObject->result = $resultModel;
        } else {
            $resultObject->result = new BaseResponse;
            $resultObject->result->message = "Validation of the model failed. ";
        }
        return $resultObject;
    }

    public function DeleteNaviaddress(string $version, string $container, string $naviaddress, string $apiVersion = null)
    {
        $client = new Client();
        $request = $client->createRequest()
            ->setMethod('DELETE')
            ->setUrl('https://staging-api.naviaddress.com/api/v' . $version . '/Addresses/' . $container . '/' . $naviaddress)
            ->addHeaders(['content-type' => 'application/json']);
        if (!is_null($apiVersion)) {
            $request->addHeaders(['api-version' => $apiVersion]);
        }
        $response = $request->send();
        $resultObject = new ResultObject;
        $resultModel = null;
        if ($response->isOk) {
            $resultModel = new NaviaddressModel;
        } else {
            $resultModel = new BaseResponse;
        }
        $resultModel->attributes = $response->data;
        if ($resultModel->validate()) {
            $resultObject->isOk = true;
            $resultObject->result = $resultModel;
        } else {
            $resultObject->result = new BaseResponse;
            $resultObject->result->message = "Validation of the model failed. ";
        }
        return $resultObject;
    }


    //методы не соответствует документации к api
    public function getToken(string $email, string $password)
    {
        $client = new Client();
        $request = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('https://staging-api.naviaddress.com/api/v' . self::$version_ . '/Sessions/')
            ->setFormat(Client::FORMAT_JSON)->addData(['email' => $email, 'password' => $password]);
        $response = $request->send();
        $resultObject = new ResultObject;
        $resultModel = null;
        if ($response->isOk) {
            $resultModel = new ProfileResponseModel;
        } else {
            $resultModel = new BaseResponse;
        }
        $resultModel->attributes = $response->data;
        if ($resultModel->validate()) {
            $resultObject->isOk = true;
            $resultObject->result = $resultModel;
        } else {
            $resultObject->result = new BaseResponse;
            $resultObject->result->message = "Validation of the model failed. ";
        }
        return $resultObject;
    }

    public function createNaviaddress($token, $lat, $lng)
    {
        $client = new Client();
        $request = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('https://staging-api.naviaddress.com/api/v' . self::$version_ . '/addresses/')
            ->setFormat(Client::FORMAT_JSON)->addData(['lat' => $lat, 'lng' => $lng, 'address_type' => 'free', 'default_lang' => 'ru'])
            ->addHeaders(['auth-token' => $token]);
        $response = $request->send();
        $resultObject = new ResultObject;
        $resultModel = null;
        if ($response->isOk) {
            $resultModel = new NaviaddressModel;
        } else {
            $resultModel = new BaseResponse;
        }
        $resultModel->attributes = $response->data['result'];
        if ($resultModel->validate()) {
            $resultObject->isOk = true;
            $resultObject->result = $resultModel;
        } else {
            $resultObject->result = new BaseResponse;
            $resultObject->result->message = "Validation of the model failed. ";
        }
        return $resultObject;
    }

    public function acceptNaviaddress($token, $container, $naviaddress)
    {
        $client = new Client();
        $request = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('https://staging-api.naviaddress.com/api/v' . self::$version_ . '/addresses/accept/' . $container . '/' . $naviaddress)
            ->setFormat(Client::FORMAT_JSON)
            ->addHeaders(['auth-token' => $token]);
        $response = $request->send();
        $resultObject = new ResultObject;
        $resultModel = null;
        if ($response->isOk) {
            $resultModel = new NaviaddressModel;
        } else {
            $resultModel = new BaseResponse;
        }
        $resultModel->attributes = $response->data['result'];
        if ($resultModel->validate()) {
            $resultObject->isOk = true;
            $resultObject->result = $resultModel;
        } else {
            $resultObject->result = new BaseResponse;
            $resultObject->result->message = "Validation of the model failed. ";
        }
        return $resultObject;
    }

    public function putNaviaddress($token, $container, $naviaddress, $name, $description)
    {
        $client = new Client();
        $request = $client->createRequest()
            ->setMethod('PUT')
            ->setUrl('https://staging-api.naviaddress.com/api/v' . self::$version_ . '/addresses/' . $container . '/' . $naviaddress.'?lang=ru')
            ->setFormat(Client::FORMAT_JSON)
            ->addData(['name'=>$name, 'description'=>$description])
            ->addHeaders(['auth-token' => $token]);
        $response = $request->send();
        $resultObject = new ResultObject;
        $resultModel = null;
        if ($response->isOk) {
            $resultModel = new NaviaddressModel;
        } else {
            $resultModel = new BaseResponse;
        }
        $resultModel->attributes = $response->data;
        if ($resultModel->validate()) {
            $resultObject->isOk = true;
            $resultObject->result = $resultModel;
        } else {
            $resultObject->result = new BaseResponse;
            $resultObject->result->message = "Validation of the model failed. ";
        }
        return $resultObject;
    }
}



class ResultObject extends BaseObject
{
    public $isOk;
    public $result;


    public function init()
    {
        parent::init();
        $this->isOk = false;
    }
}

//Models

//Address model
class GetAddressModel extends Model
{
    /*
     * string
     * maxLength: 18
     * Address container
     */
    public $container;
    /*
     * string
     * maxLength: 18
     * Naviaddress
     */
    public $naviaddress;
    /*
     * string
     * Language from client
     */
    public $lang;
    /*
     * string
     * Client’s locale from header
     */
    public $locale;


    public function rules()
    {
        return [
            [['container', 'naviaddress'], 'required'],

            [['container', 'naviaddress'], 'string', 'max' => 18],

            [['lang', 'locale'], 'string'],
        ];
    }
}

//Ensures base response structure for consistency
class BaseResponse extends Model
{
    /*
     * string
     */
    public $message;


    public function rules()
    {
        return [
            ['message', 'string'],
        ];
    }
}

class UpdateAddressModel extends Model
{
    /*
     * string
     * maxLength: 1000
     */
    public $name;
    /*
     * string
     * maxLength: 1024
     */
    public $description;
    /*
     * BookingDetails
     */
    public $booking;
    /*
     * string
     * maxLength: 18
     */
    public $naviaddress;
    /*
     * string
     * maxLength: 18
     */
    public $container;
    /*
     * JsonPoint
     */
    public $point;
    /*
     * Array of Contacts
     */
    public $contacts;
    /*
     * string($date-time)
     */
    public $event_start;
    /*
     * string($date-time)
     */
    public $event_end;
    /*
     * AddressDescriptionDetails
     */
    public $address_description;
    /*
     * LastMileDetails
     */
    public $last_mile;
    /*
     * string
     * maxLength: 1000
     */
    public $postal_address;
    /*
     * Array of Cover
     */
    public $cover;
    /*
     * Array of Cover
     */
    public $sharable_cover;
    /*
     * Array of WorkingHours
     */
    public $working_hours;
    /*
     * boolean
     */
    public $map_visibility;
    /*
     * Category
     */
    public $category;
    /*
     * string
     */
    public $default_lang;
    /*
     * string
     */
    public $lang;


    public function rules()
    {
        return [
            ['name', 'required'],

            [['name', 'postal_address'], 'string', 'max' => 1000],

            ['description', 'string', 'max' => 1024],

            [['container', 'naviaddress'], 'string', 'max' => 18],

            [['event_start', 'event_end'], 'default', 'value' => null],
            [['event_start', 'event_end'], 'datetime'],

            ['map_visibility', 'boolean'],

            [['lang', 'default_lang'], 'string'],

            [['booking', 'point', 'address_description', 'last_mile', 'cover', 'sharable_cover', 'working_hours', 'category'], 'safe'],
        ];
    }
}

class BookingDetails extends Model
{
    /*
     * string
     * maxLength: 2000
     */
    public $website;
    /*
     * string
     * maxLength: 20
     * It is the text on button ('Booking’, ‘Забронировать’)
     */
    public $caption;
    /*
     * string
     * maxLength: 15
     */
    public $telephone;


    public function rules()
    {
        return [
            ['website', 'string', 'max' => 2000],

            ['caption', 'string', 'max' => 20],

            ['telephone', 'string', 'max' => 15],
        ];
    }
}

class JsonPoint extends Model
{
    /*
     * number($double)
     */
    public $lat;
    /*
     * number($double)
     */
    public $lng;


    public function rules()
    {
        return [
            ['lat', 'double'],

            ['lng', 'double'],
        ];
    }
}

class Contacts extends Model
{
    /*
     * string
     * maxLength: 50
     * type such as phone, email, website, instagram, facebook, vkontakte
     */
    public $type;
    /*
     * string
     * maxLength: 2000
     * It is contact information by current type
     * for example: "7 (499) 253-11-83", "https://instagram.com/naviaddress", “https://facebook.com/naviaddress”
     */
    public $value;


    public function rules()
    {
        return [
            ['type', 'string', 'max' => 50],

            ['value', 'string', 'max' => 2000],
        ];
    }
}

/*
 * example:
 * {"ru": {"floor": "9", "building": "4", "apartment": "133"}}
 * {"isOffice": true, "apartment": "14555 W Indian School Rd Ste #420 Goodyear AZ 85395-9210 United States"}
 * {"building": "ST5", "intercom": "Gulraiz 3", "apartment": "House#58B"}
 */
class AddressDescriptionDetails extends Model
{
    /*
     * string
     * maxLength: 4
     */
    public $floor;
    /*
     * string
     * maxLength: 50
     */
    public $building;
    /*
     * string
     * maxLength: 10000
     * It is postal address or number of apartment
     */
    public $apartment;
    /*
     * string
     * maxLength: 50
     */
    public $intercom;
    /*
     * boolean
     */
    public $isoffice;


    public function rules()
    {
        return [
            [['building', 'intercom'], 'string', 'max' => 50],

            ['floor', 'string', 'max' => 4],

            ['apartment', 'string', 'max' => 10000],

            ['isoffice', 'boolean'],
        ];
    }
}

class LastMileDetails extends Model
{
    /*
     * string
     * maxLength: 10000
     * information about how you can come at address (case when last mile description without images)
     */
    public $text;
    /*
     * string
     * maxLength: 20
     * There are types such as text, steps
     */
    public $type;
    /*
     * Array of Step
     */
    public $steps;


    public function rules()
    {
        return [
            ['type', 'string', 'max' => 20],

            ['text', 'string', 'max' => 10000],

            ['steps', 'safe'],
        ];
    }
}

class Cover extends Model
{
    /*
     * string
     * maxLength: 36
     * It is guid like “f2285f84-ea40-4bde-94ce-2d39b34b1d5c”
     */
    public $image_uuid;
    /*
     * string
     * maxLength: 2000
     * It is url, where you get image
     */
    public $image;


    public function rules()
    {
        return [
            ['image_uuid', 'string', 'max' => 36],

            ['image', 'string', 'max' => 2000],
        ];
    }
}

class WorkingHours extends Model
{
    /*
     * string
     * maxLength: 15
     * It seems like “10:00”
     */
    public $open_time;
    /*
     * string
     * maxLength: 15
     * It seems like “10:00”
     */
    public $close_time;
    /*
     * string
     * maxLength: 15
     * It seems like “10:00”
     */
    public $break_start_time;
    /*
     * string
     * maxLength: 15
     * It seems like “10:00”
     */
    public $break_end_time;
    /*
     * Array of string
     */
    public $days;


    public function rules()
    {
        return [
            [['open_time', '$close_time', '$break_start_time', '$break_end_time'], 'string', 'max' => 15],

            ['days', 'safe'],
        ];
    }
}

class Category extends Model
{
    /*
     * integer($int32)
     */
    public $id;
    /*
     * string
     */
    public $dbid;
    /*
     * string
     * maxLength: 50
     * TODO: delete in next version
     */
    public $name;
    /*
     * string
     * maxLength: 50
     * It is text that show like category name
     */
    public $additional_name;


    public function rules()
    {
        return [
            ['id', 'integer'],

            ['dbid', 'string'],

            [['name', 'additional_name'], 'string', 'max' => 50],
        ];
    }
}

class Step extends Model
{
    /*
     * string
     * maxLength: 1000
     * It is text under image for current step
     */
    public $text;
    /*
     * string
     * maxLength: 2000
     * It is url, where you get image
     */
    public $image;
    /*
     * string
     * maxLength: 36
     * It is guid like “f2285f84-ea40-4bde-94ce-2d39b34b1d5c”
     */
    public $image_uuid;


    public function rules()
    {
        return [
            ['text', 'string', 'max' => 1000],

            ['image', 'string', 'max' => 2000],

            ['image_uuid', 'string', 'max' => 36],
        ];
    }
}

//NaviaddressModel
class NaviaddressModel extends Model
{
    /*
     * string
     * maxLength: 18
     * Container
     */
    public $container;
    /*
     * string
     * maxLength: 18
     * Naviaddress
     */
    public $naviaddress;


    public function rules()
    {
        return [
            [['container', 'naviaddress'], 'string', 'max' => 18],
        ];
    }
}

//Search address model
class SearchAddressesModel extends Model
{
    /*
     * number($double)
     * Latitude
     */
    public $latitude;
    /*
     * number($double)
     * Longitude
     */
    public $longitude;
    /*
     * query*	string
     * maxLength: 50
     * Query
     */
    public $query;
    /*
     * integer($int32)
     * maximum: 1000
     * minimum: 1
     * Limit
     */
    public $limit;
    /*
     * Array of string
     * readOnly: true
     * AddressTypes
     */
    public $addressTypes;
    /*
     * string
     * maxLength: 9
     * Address types from response
     */
    public $addressTypesFromResponse;
    /*
     * string
     * Lang
     */
    public $lang;


    public function rules()
    {
        return [
            [['latitude', 'longitude'], 'double'],

            ['query', 'string', 'max' => 50],

            ['limit', 'integer'],

            ['limit', 'compare', 'compareValue' => 1, 'operator' => '>=', 'type' => 'number'],

            ['limit', 'compare', 'compareValue' => 1000, 'operator' => '<=', 'type' => 'number'],

            ['addressTypesFromResponse', 'string', 'max' => 9],

            ['lang', 'string'],

            ['addressTypes', 'safe'],
        ];
    }
}

//Container model
class GetContainerModel extends Model
{
    /*
     * number($double)
     * Latitude
     */
    public $latitude;
    /*
     * number($double)
     * Longitude
     */
    public $longitude;


    public function rules()
    {
        return [
            [['latitude', 'longitude'], 'double'],
        ];
    }
}

//Get postal address model
class GetPostalAddressModel extends Model
{
    /*
     * number($double)
     * Latitude
     */
    public $latitude;
    /*
     * number($double)
     * Longitude
     */
    public $longitude;
    /*
     * string
     * Lang
     */
    public $lang;


    public function rules()
    {
        return [
            [['latitude', 'longitude'], 'double'],

            ['lang', 'string'],
        ];
    }
}

//Favorites Model
class GetFavoritesModel extends Model
{
    /*
     * integer($int32)
     * User Id
     */
    public $userId;
    /*
     * string
     * Lang
     */
    public $lang;


    public function rules()
    {
        return [
            ['userId', 'integer'],

            ['lang', 'string'],
        ];
    }
}

//Request address model
class RequestAddressModel extends Model
{
    /*
     * integer($int32)
     * User Id
     */
    public $userId;
    /*
     * string
     * Lang
     */
    public $lang;


    public function rules()
    {
        return [
            ['userId', 'integer'],

            ['lang', 'string'],
        ];
    }
}

class CreateAddressModel extends Model
{
    /*
     * number($double)
     */
    public $lat;
    /*
     * number($double)
     */
    public $lng;
    /*
     * string
     */
    public $default_lang;
    /*
     * string
     * maxLength: 9
     */
    public $address_type;


    public function rules()
    {
        return [
            [['lat', 'lng'], 'double'],

            ['default_lang', 'string'],

            ['address_type', 'string', 'max' => 9],
        ];
    }
}

class Message extends Model
{
    /*
     * string
     * maxLength: 1000
     */
    public $text;
    /*
     * integer($int32)
     */
    public $nps;

    public function rules()
    {
        return [
            ['nps', 'integer'],

            ['text', 'string', 'max' => 1000],
        ];
    }
}

class LoadedFile extends Model
{
    /*
     * string
     */
    public $name;
    /*
     * string($byte)
     */
    public $file;


    public function rules()
    {
        return [
            [['name', 'file'], 'string'],
        ];
    }
}

//Address images model
class AddressImagesModel extends Model
{
    /*
     * Array of LoadedFile
     */
    public $files;


    public function rules()
    {
        return [
            ['files', 'safe'],
        ];
    }
}

//Map address model
class MapAddressModelEntity extends Model
{
    /*
     * integer($int32)
     * Id
     */
    public $id;
    /*
     * string
     * AddressType
     */
    public $addressType;
    /*
     * string
     * Naviaddress
     */
    public $naviaddress;
    /*
     * string
     * Container
     */
    public $container;
    /*
     * Category
     */
    public $category;
    /*
     * PostgisPoint
     */
    public $point;
    /*
     * integer($int32)
     * ZoomLevel
     */
    public $zoomLevel;


    public function rules()
    {
        return [
            [['id', 'zoomLevel'], 'integer'],

            [['addressType', 'naviaddress', 'container'], 'string'],

            [['category', 'point'], 'safe'],
        ];
    }
}

class PostgisPoint extends Model
{
    /*
     * number($double)
     * readOnly: true
     */
    public $x;
    /*
     * number($double)
     * readOnly: true
     */
    public $y;
    /*
     * integer($int32)
     */
    public $srid;


    public function rules()
    {
        return [
            [['x', 'y'], 'double'],

            ['srid', 'integer'],
        ];
    }
}

class PartnerAddressModel extends Model
{
    /*
     * integer($int32)
     */
    public $id;
    /*
     * integer($int32)
     */
    public $user_id;
    /*
     * integer($int32)
     */
    public $priority;
    /*
     * PostgisPoint
     */
    public $point;
    /*
     * string
     */
    public $default_lang;
    /*
     * boolean
     */
    public $map_visibility;
    /*
     * string
     */
    public $container;
    /*
     * string
     */
    public $naviaddress;
    /*
     * string
     */
    public $alias;
    /*
     * string
     */
    public $address_type;
    /*
     * string
     */
    public $address_sub_type;
    /*
     * string
     */
    public $status;
    /*
     * Category
     */
    public $category;
    /*
     * Array of Cover
     */
    public $cover;
    /*
     * string($date-time)
     */
    public $updated_at;
    /*
     * string($date-time)
     */
    public $created_at;
    /*
     * LanguagesStore[String]
     */
    public $name_json;
    /*
     * LanguagesStore[String]
     */
    public $desc_json;
    /*
     * LanguagesStore[String]
     */
    public $postal_address;
    /*
     * LanguagesStore[LastMileDetails]
     */
    public $last_mile;


    public function rules()
    {
        return [
            [['id', 'user_id', 'priority'], 'integer'],

            [['default_lang', 'container', 'naviaddress', 'alias', 'address_type', 'address_sub_type', 'status'], 'string'],

            [['updated_at', 'created_at'], 'default', 'value' => null],
            [['updated_at', 'created_at'], 'datetime'],

            ['map_visibility', 'boolean'],

            [['category', 'point', 'cover', 'name_json', 'desc_json', 'postal_address', 'last_mile'], 'safe'],
        ];
    }
}

//LanguagesStore[String]
class LanguagesStoreString extends Model
{
    /*
     * string
     */
    public $ru;
    /*
     * string
     */
    public $en;
    /*
     * string
     */
    public $de;
    /*
     * string
     */
    public $fr;
    /*
     * string
     */
    public $es;
    /*
     * string
     */
    public $it;
    /*
     * string
     */
    public $el;
    /*
     * string
     */
    public $zh;


    public function rules()
    {
        return [
            [['ru', 'en', 'de', 'fr', 'es', 'it', 'el', 'zh'], 'string'],
        ];
    }
}

//LanguagesStore[LastMileDetails]
class LanguagesStoreLastMileDetails extends Model
{
    /*
     * LastMileDetails
     */
    public $ru;
    /*
     * LastMileDetails
     */
    public $en;
    /*
     * LastMileDetails
     */
    public $de;
    /*
     * LastMileDetails
     */
    public $fr;
    /*
     * LastMileDetails
     */
    public $es;
    /*
     * LastMileDetails
     */
    public $it;
    /*
     * LastMileDetails
     */
    public $el;
    /*
     * LastMileDetails
     */
    public $zh;


    public function rules()
    {
        return [
            [['ru', 'en', 'de', 'fr', 'es', 'it', 'el', 'zh'], 'safe'],
        ];
    }
}

//ProfileResponseModel
class ProfileResponseModel extends Model
{
    /*
     * string
     * Email
     */
    public $email;
    /*
     * string
     * MobilePhone
     */
    public $mobile_phone;
    /*
     * string
     * FirstName
     */
    public $first_name;
    /*
     * string
     * LastName
     */
    public $last_name;
    /*
     * boolean
     * IsGuest
     */
    public $guest;
    /*
     * integer($int32)
     * Id
     */
    public $id;
    /*
     * string
     * Token
     */
    public $token;
    /*
     * string
     * Avatar
     */
    public $avatar;
    /*
     * number($double)
     * FbUid
     */
    public $fb_uid;
    /*
     * boolean
     */
    public $is_confirmed;
    /*
     * ProfilePermissionsModel
     */
    public $permissions;
    /*
     * boolean
     */
    public $gdpr_is_receive_newsletter;
    /*
     * boolean
     */
    public $gdpr_is_restrict_user_data;
    /*
     * boolean
     */
    public $gdpr_is_collect_analytic_data;
    /*
     * boolean
     */
    public $is_nps_sent;
    /*
     * integer($int32)
     */
    public $nps;


    public function rules()
    {
        return [
            [['id', 'nps'], 'integer'],

            [['email', 'mobile_phone', 'first_name', 'last_name', 'token', 'avatar'], 'string'],

            ['fb_uid', 'double'],

            [['guest', 'is_confirmed', 'gdpr_is_receive_newsletter', 'gdpr_is_restrict_user_data', 'gdpr_is_collect_analytic_data', 'is_nps_sent'], 'boolean'],

            ['permissions', 'safe'],
        ];
    }
}

class ProfilePermissionsModel extends Model
{
    /*
     * integer($int32)
     */
    public $free_addresses_limit;
    /*
     * integer($int32)
     */
    public $event_addresses_limit;


    public function rules()
    {
        return [
            [['free_addresses_limit', 'event_addresses_limit'], 'integer'],
        ];
    }
}

class IFormFile extends Model
{
    /*
     * string
     * readOnly: true
     */
    public $contentType;
    /*
     * string
     * readOnly: true
     */
    public $contentDisposition;
    /*
     * Object of array of string
     */
    public $headers;
    /*
     * integer($int64)
     * readOnly: true
     */
    public $length;
    /*
     * string
     * readOnly: true
     */
    public $name;
    /*
     * string
     * readOnly: true
     */
    public $fileName;


    public function rules()
    {
        return [
            ['length', 'integer'],

            [['contentType', 'contentDisposition', 'name', 'fileName'], 'string'],

            ['headers', 'safe'],
        ];
    }
}

//Request export profile model
class RequestExportProfileModel extends Model
{
    /*
     * string
     * Email
     */
    public $email;
    /*
     * string
     * FileType
     */
    public $file_type;


    public function rules()
    {
        return [
            [['email', 'file_type'], 'string'],
        ];
    }
}
