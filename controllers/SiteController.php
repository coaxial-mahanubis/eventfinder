<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Event;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $coord = new LatLng(['lat' => 55.751397, 'lng' => 37.616883]);

        $map = new Map([
            'center' => $coord,
            'zoom' => 14,
        ]);

// Lets add a marker now
        $coords = [
            new LatLng(['lat' => 55.751827, 'lng' => 37.623384]),
            new LatLng(['lat' => 55.751415, 'lng' => 37.609469]),
        ];
        $markers = [
            new Marker([
            'position' => $coords[0],
            'title' => 'Васильевский спуск',
            ]),
            new Marker([
                'position' => $coords[1],
                'title' => 'Библиотека',
            ]),
        ];

        $descriptions = [
            $this->renderPartial('event', ['event'=>1]),
            $this->renderPartial('event', ['event'=>2])
        ];
        for($i = 0; $i < count($descriptions); $i++) {
            $descriptions[$i] = str_replace(array("\r", "\n"), array("", ""), $descriptions[$i]);
        }


// Provide a shared InfoWindow to the marker
        $markers[0]->attachInfoWindow(
            new InfoWindow([
                'content' => '<p>Концерт "Наши в городе".</p>'
            ])
        );
        $markers[0]->addEvent(new Event(["trigger"=>"click", "js"=>"document.getElementById('event_description').innerHTML = '".$descriptions[0]."';"]));
        $markers[1]->attachInfoWindow(
            new InfoWindow([
                'content' => '<p>День поэзии.</p>'
            ])
        );
        $markers[1]->addEvent(new Event(["trigger"=>"click", "js"=>"document.getElementById('event_description').innerHTML = '".$descriptions[1]."';"]));

// Add marker to the map
        $map->addOverlay($markers[0]);
        $map->addOverlay($markers[1]);


        return $this->render('index', [
            'map' => $map,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
