<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\AddEventForm;
use app\models\UserEvent;
use app\models\VisitRequest;
use yii\filters\VerbFilter;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Event;
use app\components\NaviComponent;



class ProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['add-event', 'delete-event', 'edit-event', 'list-events', 'organizer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-event' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays form for adding event.
     *
     * @return string
     */
    public function actionAddEvent()
    {
        //$navi = new NaviComponent();
        //echo $navi->getToken("tolik_azo@mail.ru", "pastor007")->result->token;

        $model = new AddEventForm();
        if ($model->load(Yii::$app->request->post()) && $model->send()) {
            //return $this->goBack();
        }

        $model = new AddEventForm();

        $coord = new LatLng(['lat' => 55.751397, 'lng' => 37.616883]);
        $map = new Map([
            'center' => $coord,
            'zoom' => 14,
        ]);
        $marker = new Marker([
            'position' => $coord,
            'title' => 'Ваше место',
            'draggable'=>true,
        ]);
        $marker->addEvent(new Event(["trigger"=>"mouseup", "js"=>"function(e) {document.getElementById('addeventform-lat').value = e.latLng.lat();}", 'wrap'=>false]));
        $marker->addEvent(new Event(["trigger"=>"mouseup", "js"=>"function(e) {document.getElementById('addeventform-lng').value = e.latLng.lng();}", 'wrap'=>false]));
        // Provide a shared InfoWindow to the marker
        $marker->attachInfoWindow(
            new InfoWindow([
                'content' => '<p>Ваше мероприятие.</p>'
            ])
        );
        // Add marker to the map
        $map->addOverlay($marker);

        return $this->render('add-event', [
            'model' => $model, 'map'=>$map,
        ]);
    }

    /**
     * Delete event.
     *
     * @return Response|string
     */
    public function actionDeleteEvent()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays form for editing event.
     *
     * @return string
     */
    public function actionEditEvent()
    {
        return $this->render('about');
    }

    /**
     * Displays list users events.
     *
     * @return string
     */
    public function actionListEvents()
    {
        $events = UserEvent::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->orderBy('id')
            ->all();
        return $this->render('list-events', [
            'events' => $events,
        ]);
    }

    /**
     * Displays organizer.
     *
     * @return string
     */
    public function actionOrganizer()
    {
        $events = VisitRequest::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->orderBy('id')
            ->all();
        return $this->render('organizer', [
            'events' => $events,
        ]);
    }
}
