<?php

use yii\db\Migration;

/**
 * Class m180925_200632_personal_events_tables
 */
class m180925_200632_personal_events_tables extends Migration
{
    public function up()
    {
        $this->createTable('user_events', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'naviaddress' => $this->string(),
        ]);
        $this->createTable('visit_requests', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'naviaddress' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('user_events');
        $this->dropTable('visit_requests');
    }
}
