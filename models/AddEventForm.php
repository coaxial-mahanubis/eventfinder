<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\components\NaviComponent;

/**
 * AddEventForm is the model behind the adding event form.
 */
class AddEventForm extends Model
{
    public $name;
    public $description;
    public $lat;
    public $lng;
    public $contact;
    public $start_datetime;
    public $end_datetime;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'description', 'lat', 'lng', 'start_datetime'], 'required'],
            [['name', 'description', 'start_datetime', 'end_datetime'], 'string'],
            ['contact', 'safe'],
        ];
    }

    /**
     * Sending event to naviaddress.
     * @return bool whether the event is sended in successfully
     */
    public function send()
    {
        if ($this->validate()) {
            //выполняем действия по добавлению мероприятия в naviaddress
            $navi = new NaviComponent();
            //$token = $navi->getToken("tolik_azo@mail.ru", "pastor007")->result->token;
            //$naviaddress = $navi->createNaviaddress($token, $this->lat, $this->lng)->result;
            //$naviaddress = $navi->acceptNaviaddress($token, $naviaddress->container, $naviaddress->naviaddress)->result;
            //$navi->putNaviaddress($token, $naviaddress->container, $naviaddress->naviaddress, $this->name, $this->description);
            return true;
        }
        return false;
    }
}
