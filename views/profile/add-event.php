<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\AddEventForm */
/* @var $map dosamigos\google\maps\Map */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\datetime\DateTimePicker;
use dosamigos\google\maps\Map;

$this->title = 'Добавить новое событие';
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Пожалуйста, заполните форму:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'add-event-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label('Название:') ?>

    <?= $form->field($model, 'description')->textarea()->label('Описание:') ?>

    <?= "<p>Укажите место проведения мероприятия:</p>".$map->display() ?>

    <?= $form->field($model, 'lat')->hiddenInput(['value'=>'55.751397'])->label('') ?>

    <?= $form->field($model, 'lng')->hiddenInput(['value'=>'37.616883'])->label('') ?>

    <?= $form->field($model, 'contact')->textInput()->label('Контакты:') ?>

    <?= $form->field($model, 'start_datetime')->widget(DateTimePicker::className(),[
    'name' => 'dp_1',
    'type' => DateTimePicker::TYPE_INPUT,
    'options' => ['placeholder' => 'Ввод даты/времени...'],
    'convertFormat' => true,
    'value'=> date("d.m.Y h:i",(integer) $model->start_datetime),
    'pluginOptions' => [
    'format' => 'dd.MM.yyyy hh:i',
    'autoclose'=>true,
    'weekStart'=>1, //неделя начинается с понедельника
    'startDate' => '01.05.2015 00:00', //самая ранняя возможная дата
    'todayBtn'=>true, //снизу кнопка "сегодня"
    ]
    ])->label('Начало:');?>

    <?= $form->field($model, 'end_datetime')->widget(DateTimePicker::className(),[
        'name' => 'dp_2',
        'type' => DateTimePicker::TYPE_INPUT,
        'options' => ['placeholder' => 'Ввод даты/времени...'],
        'convertFormat' => true,
        'value'=> date("d.m.Y h:i",(integer) $model->end_datetime),
        'pluginOptions' => [
            'format' => 'dd.MM.yyyy hh:i',
            'autoclose'=>true,
            'weekStart'=>1, //неделя начинается с понедельника
            'startDate' => '01.05.2015 00:00', //самая ранняя возможная дата
            'todayBtn'=>true, //снизу кнопка "сегодня"
        ]
    ])->label('Конец:');?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'add-event-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
