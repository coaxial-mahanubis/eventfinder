<?php

/* @var $this yii\web\View */
/* @var $events array of app\models\UserEvent */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

$this->title = 'Ваши мероприятия';
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ArrayDataProvider([
    'allModels' => $events,
    'pagination' => [
        'pageSize' => 10,
    ],
    'sort' => [
        'attributes' => ['id'],
    ],
]);
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Добавленные Вами мероприятия:</p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => 'Нет мероприятий.',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
             'header' => '№',],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'naviaddress',
                'format' => 'text',
                'label' => 'Мероприятие',
            ],
            ['class' => 'yii\grid\ActionColumn',
             'header'=>'Действия', ],
            // ...
        ],
    ]) ?>

</div>
