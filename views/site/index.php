<?php

use dosamigos\google\maps\Map;

/* @var $this yii\web\View */
/* @var $map dosamigos\google\maps\Map */

$this->title = 'EventFinder - все события мира на Вашей ладони';
?>
<div class="site-index">
    <div class="row">
        <?php
        // Display the map -finally :)
        $map->width="100%";
        echo $map->display();
        ?>
    </div>
    <div id="event_description" class="row">

    </div>
</div>
